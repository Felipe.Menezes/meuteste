@extends('templates.template')

@section('content')

<h1 class="text-center">Lista cliente</h1>

<div class="text-center botao-cadastro">
    <a href="{{url('cliente/create')}}">
        <button class="btn btn-success">Cadastrar</button>
    </a>
</div>
@csrf
<table class="table text-center">
    <thead>
      <tr class="titulo-tabela">
        <th scope="col">ID</th>
        <th scope="col">Nome</th>
        <th scope="col">Email</th>
        <th scope="col">Telefone</th>
        <th scope="col">CPF</th>
        <th scope="col">Ações</th>
    </tr>
    </thead>
    <tbody>

    @foreach($cliente as $clientes)

      <tr>
        <th scope="row">{{$clientes->id}}</th>
        <td>{{$clientes->name}}</td>
        <td>{{$clientes->email}}</td>
        <td>{{$clientes->telefone}}</td>
        <td>{{$clientes->cpf}}</td>
        <td>
            <a href="{{url("cliente/$clientes->id")}}">
                <button class="btn btn-dark">Visualizar</button>
            </a>
            <a href="{{url("cliente/$clientes->id/edit")}}">
                <button class="btn btn-primary">Editar</button>
            </a>
            <a href="{{url("cliente/$clientes->id")}}" class="js-del">
                <button class="btn btn-danger">Deletar</button>
            </a>
        </td>
      </tr>

      @endforeach

    </tbody>
  </table>
  {{$cliente->links()}}
@endsection