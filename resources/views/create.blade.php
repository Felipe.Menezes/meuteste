@extends('templates.template')

@section('content')

<h1 class="text-center">@if(isset($cliente)) Editar @else Cadastrar @endif</h1>

<div>

    @if(isset($errors) && count($errors)>0)
    <div class="text-center alert-danger">
        @foreach($errors->all() as $erro)
            {{$erro}}<br>
        @endforeach
    </div>
    @endif

    @if(isset($cliente)) 
    <form name="formEdit" id="formEdit" method="post" action="{{url("cliente/$cliente->id")}}">
        @method('PUT')
    @else 
    <form name="formCad" id="formCad" method="post" action="{{url('cliente')}}">
    @endif
    
        @csrf
        <div><input type="text" name="name" id="name" placeholder="Nome" value="{{$cliente->name ?? ''}}" required></div>
        <div><input type="email" name="email" id="email" placeholder="Email" value="{{$cliente->email ?? ''}}" required></div>
        <div><input type="text" class="phone" name="telefone" id="telefone" placeholder="Telefone" value="{{$cliente->telefone ?? ''}}" required></div>
        <div><input type="text" class="cpf" name="cpf" id="cpf" placeholder="CPF" value="{{$cliente->cpf ?? ''}}" required></div>
        <div><input type="submit" value="@if(isset($cliente)) Editar @else Cadastrar @endif" class="btn btn-primary"></div>
    </form>
</div>

@endsection