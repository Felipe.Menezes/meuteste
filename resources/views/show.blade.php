@extends('templates.template')

@section('content')

<h1 class="text-center">Visualizar cliente</h1>

<div class="text-center">
    Nome: {{$cliente->name}}<br>
    Email: {{$cliente->email}}<br>
    Telefone: {{$cliente->telefone}}<br>
    CPF: {{$cliente->cpf}}<br>
</div>


@endsection