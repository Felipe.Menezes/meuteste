<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClienteRequest as RequestsClienteRequest;
use Illuminate\Http\Request;
use App\Models\ModelCliente as ModelsModelCliente;

class ClienteController extends Controller
{

    private $objCliente;

    public function __construct()
    {
        $this->objCliente = new ModelsModelCliente();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cliente=$this->objCliente->paginate(5);
        return view('index', compact('cliente'));
        // dd($this->objCliente->find(1)->clientes());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $cliente=$this->objCliente->all();
        // return view('create', compact('cliente'));
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestsClienteRequest $request)
    {
        $cad=$this->objCliente->create([
            'name'=>$request->name,
            'email'=>$request->email,
            'telefone'=>$request->telefone,
            'cpf'=>$request->cpf
        ]);
        if($cad){
            return redirect('cliente');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente=$this->objCliente->find($id);
        return view('show', compact('cliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente=$this->objCliente->find($id);
        return view('create', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestsClienteRequest $request, $id)
    {
        $this->objCliente->where(['id'=>$id])->update([
            'name'=>$request->name,
            'email'=>$request->email,
            'telefone'=>$request->telefone,
            'cpf'=>$request->cpf
        ]);
        return redirect('cliente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del=$this->objCliente->destroy($id);
        return($del)?"sim":"não";
    }
}
