<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelCliente extends Model
{
    protected $table = 'cliente';
    protected $fillable = ['name', 'email', 'telefone', 'cpf'];

    public function clientes()
    {
        return $this->all();
    }
}
