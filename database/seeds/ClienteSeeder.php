<?php

use Illuminate\Database\Seeder;
use App\Models\ModelCliente;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(ModelCliente $cliente)
    {
        $cliente->create([
            'name'=>'Cliente1',
            'email'=>'cliente1@email.com',
            'telefone'=>'11111111',
            'cpf'=>'11111111111',
        ]);

        $cliente->create([
            'name'=>'Cliente2',
            'email'=>'cliente2@email.com',
            'telefone'=>'22222222',
            'cpf'=>'222222222222',
        ]);
    }
}
